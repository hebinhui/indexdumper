package priv.mdc.index.dumper.filter;

public class FilterException extends Exception{

	private static final long serialVersionUID = 1L;

	public FilterException(String error){
		super(error);
	}
	
}
